@echo off
cls
echo Installing requirements...
pip install -r requirements.txt
:python setup.py bdist_wheel
echo Building application....
pyinstaller -D -F -n PiHome -c .\PiHome\__main__.py
echo Launching application....
.\dist\PiHome.exe
@echo on