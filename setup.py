from setuptools import setup

setup(
    name="PiHome",
    version="2.0",
    description="PiHome Application",
    author="Bradley Sheets",
    author_email="support@hexotic.net",
    url="http://hexotic.net",
    packages=["PiHome"],
    install_requires=[
        'docutils',
        'pygments',
        'pipiwin32',
        'kivy.deps.sdl2',
        'kivy.deps.gstreamer',
        'kivy.deps.angle',
        'kivy'
    ]
)