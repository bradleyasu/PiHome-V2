from kivy.app import App
from kivy.config import Config
from kivy.uix.textinput import TextInput
import imp
import os

from PiHome.comps import delay

class PiHomeApp(App):
    def build(self):
        # Load any 3rd party plugins
        plugins = self.loadPlugins()
        for plugin in plugins:
            print(plugin)
            pass

        # Build the application
        return TextInput()

    #
    # loadPlugins()
    #
    # iterates over the plugins in directory, looking for modules
    # that have implemented the correct interface and will
    # load them into memory to be added to the PiHome screen manager
    #
    def loadPlugins(self):
        # plugin directory is expected to be ./plugins/
        plugins = {}
        pluginDir = os.listdir("./plugins")
        for module in pluginDir:
            print(module)
        return plugins

def main():
    # Setup defaults 
    Config.set("graphics", "width", "800")
    Config.set("graphics", "height", "500")
    Config.set('kivy', 'keyboard_mode', 'systemanddock')
    # Launch Application
    PiHomeApp().run()