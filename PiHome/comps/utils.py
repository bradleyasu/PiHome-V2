import time
def delay(func):
    def wrapper():
        time.sleep(3)
        return func()
    return wrapper